FROM mcr.microsoft.com/dotnet/aspnet:6.0.8

RUN ["apt-get", "update"]
RUN ["apt-get", "-y", "install", "libgdiplus"]
RUN ["apt-get", "-y", "install", "xvfb", "libfontconfig", "wkhtmltopdf"]
RUN ["apt-get", "-y", "install", "libc6-dev"]
RUN ["apt-get", "-y", "install", "openssl"]
#RUN ["apt-get", "-y", "install", "postgresql-client-common"]
#RUN ["apt-get", "-y", "install", "postgresql-client"]
